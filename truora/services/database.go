package services

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"truora/common/dto"
)

// CREATE USER IF NOT EXISTS truorauser;
// CREATE DATABASE truora;
// GRANT ALL ON DATABASE truora TO truorauser;
// .\cockroach start-single-node --insecure
// .\cockroach sql --insecure

// SaveReport registra en la base de datos la información obtenida de un dominio
func SaveReport(domain dto.DomainDto) {
	fmt.Println("Guardando reporte...")

	db, err := sql.Open("postgres", "postgresql://truorauser@DESKTOP-GL1B8U9:26257/truora?sslmode=disable")
	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
	}

	if _, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS report (id UUID PRIMARY KEY DEFAULT gen_random_uuid(), domain STRING(256), report JSONB, creation_time TIMESTAMP DEFAULT now())"); err != nil {
		log.Fatal(err)
	}

	report, err := json.Marshal(domain)
	if err != nil {
		log.Fatal("Error creating json from report: ", err)
	}

	if _, err := db.Exec("INSERT INTO report (domain, report) VALUES ('" + domain.Domain + "', '" + string(report) + "')"); err != nil {
		log.Fatal(err)
	}
}

// GetLastReportByDomain retorna el último reporte de un dominio
func GetLastReportByDomain(domain string, wg *sync.WaitGroup) string {
	fmt.Println("Ejecutando GetLastReportByDomain...")
	defer wg.Done()

	db, err := sql.Open("postgres", "postgresql://truorauser@DESKTOP-GL1B8U9:26257/truora?sslmode=disable")
	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
	}

	if _, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS report (id UUID PRIMARY KEY DEFAULT gen_random_uuid(), domain STRING(256), report JSONB, creation_time TIMESTAMP DEFAULT now())"); err != nil {
		log.Fatal(err)
	}

	var report string
	rows, err := db.Query("SELECT report FROM report WHERE domain = '" + domain + "' ORDER BY creation_time DESC LIMIT 1;")
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		if err := rows.Scan(&report); err != nil {
			log.Fatal(err)
		}
	}

	return report
}

// GetDomainsConsulted retorna los dominios que han sido consultados
func GetDomainsConsulted(wg *sync.WaitGroup) []string {
	fmt.Println("Obteniendo dominios consultados...")
	defer wg.Done()

	db, err := sql.Open("postgres", "postgresql://truorauser@DESKTOP-GL1B8U9:26257/truora?sslmode=disable")
	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
	}

	rows, err := db.Query("SELECT DISTINCT(domain) FROM report ORDER BY domain")
	if err != nil {
		log.Fatal(err)
	}

	var domains []string

	defer rows.Close()

	for rows.Next() {
		var domain string
		if err := rows.Scan(&domain); err != nil {
			log.Fatal(err)
		}

		domains = append(domains, domain)
	}

	return domains
}
