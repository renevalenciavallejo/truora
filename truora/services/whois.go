package services

import (
	"fmt"
	"sync"

	"github.com/likexian/whois-go"
	whoisparser "github.com/likexian/whois-parser-go"
)

// GetIPAddressInfo retorna la información de un sitio obtenida a través del protocolo WHOIS
func GetIPAddressInfo(domain string, wg *sync.WaitGroup) whoisparser.WhoisInfo {
	fmt.Println("Ejecutando GetIPAddressInfo...")
	defer wg.Done()

	response, err := whois.Whois(domain)
	if err != nil {
		fmt.Print(err)
	}

	responseObject, err := whoisparser.Parse(response)
	if err != nil {
		fmt.Print(err)
	}

	return responseObject
}
