package services

import (
	"fmt"
	"net"
	"sync"
	"time"
)

// IsDown indica si un servidor se encuentra caído
func IsDown(domain string, wg *sync.WaitGroup) bool {
	fmt.Println("Ejecutando IsDown...")
	defer wg.Done()

	port := "80"
	seconds := 5
	timeOut := time.Duration(seconds) * time.Second

	conn, err := net.DialTimeout("tcp", domain+":"+port, timeOut)

	if err != nil {
		fmt.Println(err)
		return true
	}

	conn.Close()
	return false
}
