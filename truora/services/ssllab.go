package services

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

var client = &http.Client{Timeout: 10 * time.Second}

// GetDomainInfo retorna la información de un dominio obtenida a través del API de ssllabs
func GetDomainInfo(domain string, wg *sync.WaitGroup) string {
	fmt.Println("Ejecutando GetDomainInfo...")
	defer wg.Done()

	response, err := client.Get("https://api.ssllabs.com/api/v3/analyze?host=" + domain)
	if err != nil {
		log.Fatal(err)
	}

	defer response.Body.Close()

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	responseObject := string(responseData)
	return responseObject
}
