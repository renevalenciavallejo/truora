package services

import (
	"fmt"
	"sync"

	"github.com/gocolly/colly"
)

// GetTitleAndShortcut retorna el título y logo de una página html
func GetTitleAndShortcut(domain string, wg *sync.WaitGroup) (string, string) {
	fmt.Println("Ejecutando GetTitleAndShortcut...")
	defer wg.Done()

	var title string
	var shortcut string

	c := colly.NewCollector()

	c.OnHTML("title", func(e *colly.HTMLElement) {
		if title == "" {
			title = e.Text
		}
	})

	c.OnHTML("link[href]", func(e *colly.HTMLElement) {
		if e.Attr("rel") == "shortcut icon" {
			link := e.Attr("href")
			shortcut = link
		}
	})

	c.Visit("https://" + domain)

	return title, shortcut
}
