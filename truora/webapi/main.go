package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
	"truora/application"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	_ "github.com/lib/pq"
)

func main() {
	//fmt.Printf("%+v\n", application.GetReportByDomain("semana.com"))

	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	})

	r.Use(cors.Handler)

	r.Get("/report/{domain}", getReportHandler())
	r.Get("/reports", getReportsHandler())

	http.ListenAndServe(":3000", r)
}

func getReportHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		domain := chi.URLParam(r, "domain")

		start := time.Now()

		fmt.Println("Obteniendo reporte para: " + domain)
		report := application.GetReportByDomain(domain)

		elapsedTime := time.Since(start)
		fmt.Println("Tiempo total de ejecución: " + elapsedTime.String())
		fmt.Println("")

		json.NewEncoder(w).Encode(report)
	}
}

func getReportsHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		fmt.Println("Obteniendo historial reportes")
		domains := application.GetDomainsConsulted()

		elapsedTime := time.Since(start)
		fmt.Println("Tiempo total de ejecución: " + elapsedTime.String())
		fmt.Println("")

		json.NewEncoder(w).Encode(domains)
	}
}
