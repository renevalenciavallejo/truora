package dto

// ServerDto representa la información obtenida de un servidor
type ServerDto struct {
	Address  string `json:"ipAddress"`
	SslGrade string `json:"grade"`
	Country  string
	Owner    string
}
