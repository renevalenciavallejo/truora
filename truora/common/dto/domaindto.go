package dto

// DomainDto representa la información obtenida de un dominio
type DomainDto struct {
	Domain          string
	Servers         []ServerDto `json:"endpoints"`
	ServersChanged  bool
	SslGrade        string
	PreviusSslGrade string
	Logo            string
	Title           string
	IsDown          bool
}
