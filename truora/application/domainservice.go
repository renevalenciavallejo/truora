package application

import (
	"encoding/json"
	"reflect"
	"sort"
	"sync"
	"truora/common/dto"
	"truora/services"

	whoisparser "github.com/likexian/whois-parser-go"
)

// GetReportByDomain retorna un reporte de un dominio
func GetReportByDomain(url string) dto.DomainDto {
	var wg sync.WaitGroup
	wg.Add(5)

	var isDown bool
	go func() {
		isDown = services.IsDown(url, &wg)
	}()

	var domainInfo string
	go func() {
		domainInfo = services.GetDomainInfo(url, &wg)
	}()

	var ipAddressInfo whoisparser.WhoisInfo
	go func() {
		ipAddressInfo = services.GetIPAddressInfo(url, &wg)
	}()

	var title, shortcut string
	go func() {
		title, shortcut = services.GetTitleAndShortcut(url, &wg)
	}()

	var lastReport dto.DomainDto
	go func() {
		json.Unmarshal([]byte(services.GetLastReportByDomain(url, &wg)), &lastReport)
	}()

	wg.Wait()

	// Mapea la información del dominio

	var report dto.DomainDto
	json.Unmarshal([]byte(domainInfo), &report)

	report.Domain = url
	report.IsDown = isDown

	// Mapea la información de los servidores

	var serverGrades []string
	for i := 0; i < len(report.Servers); i++ {
		report.Servers[i].Country = ipAddressInfo.Registrant.Country
		report.Servers[i].Owner = ipAddressInfo.Registrant.Organization

		if report.Servers[i].SslGrade != "" {
			serverGrades = append(serverGrades, report.Servers[i].SslGrade)
		}
	}

	// Identifica el menor grado SSL

	if len(serverGrades) > 0 {
		sort.Strings(serverGrades)
		report.SslGrade = serverGrades[len(serverGrades)-1]
	} else {
		report.SslGrade = "No information"
	}

	// Mapea la información de título y logo

	report.Logo = shortcut
	report.Title = title

	// Compara el nuevo reporte con el último reporte

	if lastReport.Title != "" {
		report.PreviusSslGrade = lastReport.SslGrade
		report.ServersChanged = reflect.DeepEqual(report.Servers, lastReport.Servers) == false
	}

	// Guarda en la base de datos el reporte

	services.SaveReport(report)

	return report
}

// GetDomainsConsulted retona los dominios que han sido consultados
func GetDomainsConsulted() []string {
	var wg sync.WaitGroup
	wg.Add(1)

	var domains []string
	go func() {
		domains = services.GetDomainsConsulted(&wg)
	}()

	wg.Wait()
	return domains
}
